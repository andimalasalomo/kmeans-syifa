-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2018 at 10:27 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kmeans_syifa`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `hash`) VALUES
(1, 'admin', '$2y$10$/jowgkDhe4I9wL0T5WZkIuJW4zim72QN4ngN0O87m5z5H8lNP5tSq');

-- --------------------------------------------------------

--
-- Table structure for table `data_hiv_aids`
--

CREATE TABLE `data_hiv_aids` (
  `id_data` int(11) NOT NULL,
  `provinsi` varchar(255) NOT NULL,
  `hiv` int(255) NOT NULL,
  `aids` int(255) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_hiv_aids`
--

INSERT INTO `data_hiv_aids` (`id_data`, `provinsi`, `hiv`, `aids`, `tahun`) VALUES
(1, 'Aceh', 32, 32, 2011),
(2, 'Sumatera Utara', 1251, 30, 2011),
(3, 'Sumatera Barat', 132, 130, 2011),
(4, 'Riau', 439, 118, 2011),
(5, 'Jambi', 105, 47, 2011),
(6, 'Sumatera Selatan', 265, 41, 2011),
(7, 'Bengkulu', 33, 18, 2011),
(8, 'Lampung', 295, 11, 2011),
(9, 'Kepulauan Bangka Belitung', 103, 34, 2011),
(10, 'Kepulauan Riau', 674, 31, 2011),
(11, 'DKI Jakarta', 4012, 1332, 2011),
(12, 'Jawa Barat', 1429, 480, 2011),
(13, 'Jawa Tengah', 1057, 546, 2011),
(14, 'DI Yogyakarta', 310, 34, 2011),
(15, 'Jawa Timur', 2715, 1261, 2011),
(16, 'Banten', 433, 188, 2011),
(17, 'Bali', 1557, 567, 2011),
(18, 'Nusa Tenggara Barat', 132, 81, 2011),
(19, 'Nusa Tenggara Timur', 352, 41, 2011),
(20, 'Kalimantan Barat', 499, 160, 2011),
(21, 'Kalimantan Tengah', 68, 20, 2011),
(22, 'Kalimantan Selatan', 83, 65, 2011),
(23, 'Kalimantan Timur', 429, 91, 2011),
(24, 'Kalimantan Utara', 0, 0, 2011),
(25, 'Sulawesi Utara', 222, 133, 2011),
(26, 'Sulawesi Tengah', 37, 21, 2011),
(27, 'Sulawesi Selatan', 611, 212, 2011),
(28, 'Sulawesi Tenggara', 49, 66, 2011),
(29, 'Gorontalo', 11, 8, 2011),
(30, 'Sulawesi Barat', 5, 0, 2011),
(31, 'Maluku', 440, 3, 2011),
(32, 'Maluku Utara', 46, 42, 2011),
(33, 'Papua Barat', 356, 76, 2011),
(34, 'Papua', 2850, 1367, 2011),
(35, 'Aceh', 26, 27, 2012),
(36, 'Sumatera Utara', 1337, 260, 2012),
(37, 'Sumatera Barat', 133, 120, 2012),
(38, 'Riau', 314, 130, 2012),
(39, 'Jambi', 203, 62, 2012),
(40, 'Sumatera Selatan', 230, 62, 2012),
(41, 'Bengkulu', 40, 25, 2012),
(42, 'Lampung', 335, 137, 2012),
(43, 'Kepulauan Bangka Belitung', 132, 28, 2012),
(44, 'Kepulauan Riau', 792, 99, 2012),
(45, 'DKI Jakarta', 3926, 1187, 2012),
(46, 'Jawa Barat', 1416, 184, 2012),
(47, 'Jawa Tengah', 1110, 798, 2012),
(48, 'DI Yogyakarta', 272, 243, 2012),
(49, 'Jawa Timur', 2912, 1778, 2012),
(50, 'Banten', 395, 208, 2012),
(51, 'Bali', 1737, 708, 2012),
(52, 'Nusa Tenggara Barat', 110, 128, 2012),
(53, 'Nusa Tenggara Timur', 242, 287, 2012),
(54, 'Kalimantan Barat', 465, 98, 2012),
(55, 'Kalimantan Tengah', 46, 9, 2012),
(56, 'Kalimantan Selatan', 88, 88, 2012),
(57, 'Kalimantan Timur', 392, 53, 2012),
(58, 'Kalimantan Utara', 0, 0, 2012),
(59, 'Sulawesi Utara', 212, 144, 2012),
(60, 'Sulawesi Tengah', 86, 52, 2012),
(61, 'Sulawesi Selatan', 524, 231, 2012),
(62, 'Sulawesi Tenggara', 71, 56, 2012),
(63, 'Gorontalo', 8, 16, 2012),
(64, 'Sulawesi Barat', 7, 3, 2012),
(65, 'Maluku', 295, 117, 2012),
(66, 'Maluku Utara', 92, 79, 2012),
(67, 'Papua Barat', 535, 154, 2012),
(68, 'Papua', 3028, 2078, 2012),
(69, 'Aceh', 46, 47, 2013),
(70, 'Sumatera Utara', 1603, 41, 2013),
(71, 'Sumatera Barat', 222, 150, 2013),
(72, 'Riau', 412, 172, 2013),
(73, 'Jambi', 208, 83, 2013),
(74, 'Sumatera Selatan', 262, 0, 2013),
(75, 'Bengkulu', 79, 37, 2013),
(76, 'Lampung', 189, 94, 2013),
(77, 'Kepulauan Bangka Belitung', 97, 59, 2013),
(78, 'Kepulauan Riau', 926, 7, 2013),
(79, 'DKI Jakarta', 5865, 996, 2013),
(80, 'Jawa Barat', 3041, 33, 2013),
(81, 'Jawa Tengah', 2322, 524, 2013),
(82, 'DI Yogyakarta', 489, 134, 2013),
(83, 'Jawa Timur', 3391, 2583, 2013),
(84, 'Banten', 502, 253, 2013),
(85, 'Bali', 1690, 682, 2013),
(86, 'Nusa Tenggara Barat', 170, 83, 2013),
(87, 'Nusa Tenggara Timur', 259, 335, 2013),
(88, 'Kalimantan Barat', 525, 221, 2013),
(89, 'Kalimantan Tengah', 57, 11, 2013),
(90, 'Kalimantan Selatan', 174, 83, 2013),
(91, 'Kalimantan Timur', 467, 176, 2013),
(92, 'Kalimantan Utara', 0, 0, 2013),
(93, 'Sulawesi Utara', 264, 146, 2013),
(94, 'Sulawesi Tengah', 147, 90, 2013),
(95, 'Sulawesi Selatan', 792, 328, 2013),
(96, 'Sulawesi Tenggara', 100, 51, 2013),
(97, 'Gorontalo', 26, 16, 2013),
(98, 'Sulawesi Barat', 0, 4, 2013),
(99, 'Maluku', 236, 142, 2013),
(100, 'Maluku Utara', 54, 65, 2013),
(101, 'Papua Barat', 448, 626, 2013),
(102, 'Papua', 3974, 1891, 2013),
(103, 'Aceh', 60, 56, 2014),
(104, 'Sumatera Utara', 1628, 231, 2014),
(105, 'Sumatera Barat', 321, 240, 2014),
(106, 'Riau', 550, 171, 2014),
(107, 'Jambi', 170, 59, 2014),
(108, 'Sumatera Selatan', 252, 87, 2014),
(109, 'Bengkulu', 92, 19, 2014),
(110, 'Lampung', 256, 95, 2014),
(111, 'Kepulauan Bangka Belitung', 113, 33, 2014),
(112, 'Kepulauan Riau', 973, 44, 2014),
(113, 'DKI Jakarta', 5851, 130, 2014),
(114, 'Jawa Barat', 3740, 61, 2014),
(115, 'Jawa Tengah', 2867, 740, 2014),
(116, 'DI Yogyakarta', 614, 199, 2014),
(117, 'Jawa Timur', 4508, 1456, 2014),
(118, 'Banten', 680, 209, 2014),
(119, 'Bali', 2129, 880, 2014),
(120, 'Nusa Tenggara Barat', 149, 80, 2014),
(121, 'Nusa Tenggara Timur', 249, 389, 2014),
(122, 'Kalimantan Barat', 699, 168, 2014),
(123, 'Kalimantan Tengah', 113, 23, 2014),
(124, 'Kalimantan Selatan', 227, 76, 2014),
(125, 'Kalimantan Timur', 539, 226, 2014),
(126, 'Kalimantan Utara', 0, 36, 2014),
(127, 'Sulawesi Utara', 392, 163, 2014),
(128, 'Sulawesi Tengah', 131, 116, 2014),
(129, 'Sulawesi Selatan', 839, 279, 2014),
(130, 'Sulawesi Tenggara', 160, 54, 2014),
(131, 'Gorontalo', 24, 6, 2014),
(132, 'Sulawesi Barat', 30, 3, 2014),
(133, 'Maluku', 414, 118, 2014),
(134, 'Maluku Utara', 63, 77, 2014),
(135, 'Papua Barat', 600, 13, 2014),
(136, 'Papua', 3278, 1338, 2014),
(137, 'Aceh', 48, 49, 2015),
(138, 'Sumatera Utara', 1491, 53, 2015),
(139, 'Sumatera Barat', 243, 0, 2015),
(140, 'Riau', 586, 253, 2015),
(141, 'Jambi', 148, 52, 2015),
(142, 'Sumatera Selatan', 265, 175, 2015),
(143, 'Bengkulu', 87, 24, 2015),
(144, 'Lampung', 345, 111, 2015),
(145, 'Kepulauan Bangka Belitung', 147, 61, 2015),
(146, 'Kepulauan Riau', 885, 212, 2015),
(147, 'DKI Jakarta', 4695, 130, 2015),
(148, 'Jawa Barat', 3741, 657, 2015),
(149, 'Jawa Tengah', 3005, 963, 2015),
(150, 'DI Yogyakarta', 531, 91, 2015),
(151, 'Jawa Timur', 4155, 647, 2015),
(152, 'Banten', 649, 134, 2015),
(153, 'Bali', 2028, 957, 2015),
(154, 'Nusa Tenggara Barat', 194, 89, 2015),
(155, 'Nusa Tenggara Timur', 299, 0, 2015),
(156, 'Kalimantan Barat', 456, 179, 2015),
(157, 'Kalimantan Tengah', 134, 26, 2015),
(158, 'Kalimantan Selatan', 250, 0, 2015),
(159, 'Kalimantan Timur', 504, 254, 2015),
(160, 'Kalimantan Utara', 84, 43, 2015),
(161, 'Sulawesi Utara', 311, 180, 2015),
(162, 'Sulawesi Tengah', 138, 112, 2015),
(163, 'Sulawesi Selatan', 700, 145, 2015),
(164, 'Sulawesi Tenggara', 129, 60, 2015),
(165, 'Gorontalo', 24, 25, 2015),
(166, 'Sulawesi Barat', 13, 0, 2015),
(167, 'Maluku', 409, 62, 2015),
(168, 'Maluku Utara', 45, 104, 2015),
(169, 'Papua Barat', 702, 7, 2015),
(170, 'Papua', 3494, 226, 2015),
(171, 'Aceh', 70, 60, 2016),
(172, 'Sumatera Utara', 1891, 118, 2016),
(173, 'Sumatera Barat', 396, 152, 2016),
(174, 'Riau', 822, 252, 2016),
(175, 'Jambi', 215, 75, 2016),
(176, 'Sumatera Selatan', 346, 115, 2016),
(177, 'Bengkulu', 115, 55, 2016),
(178, 'Lampung', 381, 76, 2016),
(179, 'Kepulauan Bangka Belitung', 135, 28, 2016),
(180, 'Kepulauan Riau', 1037, 224, 2016),
(181, 'DKI Jakarta', 6019, 555, 2016),
(182, 'Jawa Barat', 5466, 382, 2016),
(183, 'Jawa Tengah', 4032, 1402, 2016),
(184, 'DI Yogyakarta', 736, 112, 2016),
(185, 'Jawa Timur', 6513, 1110, 2016),
(186, 'Banten', 1092, 191, 2016),
(187, 'Bali', 2367, 882, 2016),
(188, 'Nusa Tenggara Barat', 175, 75, 2016),
(189, 'Nusa Tenggara Timur', 487, 27, 2016),
(190, 'Kalimantan Barat', 525, 110, 2016),
(191, 'Kalimantan Tengah', 141, 59, 2016),
(192, 'Kalimantan Selatan', 454, 0, 2016),
(193, 'Kalimantan Timur', 813, 177, 2016),
(194, 'Kalimantan Utara', 163, 30, 2016),
(195, 'Sulawesi Utara', 409, 199, 2016),
(196, 'Sulawesi Tengah', 157, 72, 2016),
(197, 'Sulawesi Selatan', 993, 571, 2016),
(198, 'Sulawesi Tenggara', 134, 62, 2016),
(199, 'Gorontalo', 7, 37, 2016),
(200, 'Sulawesi Barat', 22, 9, 2016),
(201, 'Maluku', 621, 128, 2016),
(202, 'Maluku Utara', 120, 76, 2016),
(203, 'Papua Barat', 530, 0, 2016),
(204, 'Papua', 3866, 70, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL,
  `provinsi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `provinsi`) VALUES
(1, 'Aceh'),
(2, 'Sumatera Utara'),
(3, 'Sumatera Barat'),
(4, 'Riau'),
(5, 'Jambi'),
(6, 'Sumatera Selatan'),
(7, 'Bengkulu'),
(8, 'Lampung'),
(9, 'Kepulauan Bangka Belitung'),
(10, 'Kepulauan Riau'),
(11, 'DKI Jakarta'),
(12, 'Jawa Barat'),
(13, 'Jawa Tengah'),
(14, 'DI Yogyakarta'),
(15, 'Jawa Timur'),
(16, 'Banten'),
(17, 'Bali'),
(18, 'Nusa Tenggara Barat'),
(19, 'Nusa Tenggara Timur'),
(20, 'Kalimantan Barat'),
(21, 'Kalimantan Tengah'),
(22, 'Kalimantan Selatan'),
(23, 'Kalimantan Timur'),
(24, 'Kalimantan Utara'),
(25, 'Sulawesi Utara'),
(26, 'Sulawesi Tengah'),
(27, 'Sulawesi Selatan'),
(28, 'Sulawesi Tenggara'),
(29, 'Gorontalo'),
(30, 'Sulawesi Barat'),
(31, 'Maluku'),
(32, 'Maluku Utara'),
(33, 'Papua Barat'),
(34, 'Papua');

-- --------------------------------------------------------

--
-- Table structure for table `tahun`
--

CREATE TABLE `tahun` (
  `tahun` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun`
--

INSERT INTO `tahun` (`tahun`) VALUES
(2011),
(2012),
(2013),
(2014),
(2015),
(2016);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `data_hiv_aids`
--
ALTER TABLE `data_hiv_aids`
  ADD PRIMARY KEY (`id_data`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`tahun`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_hiv_aids`
--
ALTER TABLE `data_hiv_aids`
  MODIFY `id_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_provinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
