<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Lihat Data</h3>
            <p class="text-muted m-b-30">Lihat Data Penyakit HIV dan AIDS</p>
                <form action="<?php echo base_url("user/searchdata");?>" method="post" >
                <div class="box-body">
                  <div class="row">   
                    <div class="form-group col-md-6">
                    <label class="col-md-12">Tahun Awal</label>
                    <div class="col-md-12">
                        <select name="thn_awal" class="form-control" required>
                            <option value="" selected disabled>Silakan Pilih Tahun</option>
                            <?php
                                foreach ($data_tahun as $data) {
                                    $tahun = $data->tahun;
                                    echo "
                                        <option value='$data->tahun'>$data->tahun</option>
                                    ";
                                } 
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12">Tahun Akhir</label>
                    <div class="col-md-12">
                        <select name="thn_akhir" class="form-control" required>
                            <option value="" selected disabled>Silakan Pilih Tahun</option>
                            <?php
                                foreach ($data_tahun as $data) {
                                    $tahun = $data->tahun;
                                    echo "
                                        <option value='$data->tahun'>$data->tahun</option>
                                    ";
                                } 
                            ?>
                        </select>
                    </div>
                </div>
                  </div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
        </div>
    </div>
</div>
<!-- /.row -->
<?php if($viewdata==""){ }
elseif($viewdata=="error"){ ?>
        <div class="container">
          <h1 align="center">Tahun yang Di Inputkan Salah</h1>
        </div>
<?php }else{?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Data HIV & AIDS</h3>
            <p class="text-muted m-b-30">Data Penyakit Tahun <?php echo $thn_awal.' - '.$thn_akhir; ?></p
            <div class="table-responsive">
                <table id="example1" class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th class="no-sort" style="width: 350px">Provinsi</th>
                            <th class="no-sort" >HIV</th>
                            <th class="no-sort" >AIDS</th>
                            <th>Tahun</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                    $no = 1;
                    foreach ($data_edit as $data) {
                        $provinsi = $data->provinsi;
                        $hiv = $data->hiv;
                        $aids = $data->aids;
                        $tahun = $data->tahun;
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $provinsi ?></td>
                            <td><?php echo $hiv ?></td>
                            <td><?php echo $aids ?></td>
                            <td><?php echo $tahun ?></td>         
                        </tr>
                        <?php
                    }
                ?>                          
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- /.row -->