<!-- .row -->
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Tentang Website</h3>
            <p class="text-muted m-b-30 font-13">Infomasi mengenai Website</p>
            <p align="justify">Website ini berisi informasi mengenai penyakit HIV dan AIDS, jumlah penderita penyakit HIV dan AIDS pada 34 provinsi di Indonesia, dan pengelompokan data penyakit HIV dan AIDS menggunakan metode clustering dengan algoritma K-Means. Data yang digunakan adalah data penyakit HIV dan AIDS dari tahun 2011 yang bersumber dari website depkes.go.id. Website ini juga dapat menarik kesimpulan apakah tingkat penyakit HIV akan mempengaruhi tingkat AIDS di Indonesia.</p>
            <br>
           <p style="text-align: justify;">
            <b>Kontak :</b><br>
                Alamat	  		: Jl. HR. Rasuna Said, Blok X.5, Kavling 4-9, RT.1/RW.2, Kuningan, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950 <br>
				Telepon	  		: (021) 5221225 <br>              
				</p><hr>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.252694055871!2d106.83036201476915!3d-6.230380295489976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3eefc2bff31%3A0x4a9c725eacd4d99f!2sKementerian+Kesehatan+Republik+Indonesia!5e0!3m2!1sid!2sid!4v1533279138530" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>   
</div>