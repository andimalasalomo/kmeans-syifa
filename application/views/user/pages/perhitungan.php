<!-- /row -->
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Pilih Tahun</h3><br>
            <?php if($this->session->flashdata('notif')){
                $type = $this->session->flashdata('type');
                echo "<div id='alert' class='alert $type'>";
                echo $this->session->flashdata('notif').'</div>';
            } ?>
            <form class="form-horizontal" action="<?php echo base_url("user/perhitungan");?>" method="post">
                <div class="form-group col-md-12">
                    <label class="col-md-12">Tipe Data </label>
                    <div class="col-md-12">
                        <select name="tipe_data" class="form-control" required>
                            <option value="" selected disabled>Silakan Pilih Tipe Data</option>
                            <option value="aids">AIDS</option>
                            <option value="hiv">HIV</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-12">Tahun Awal</label>
                    <div class="col-md-12">
                        <select name="tahun_awal" class="form-control" required>
                            <option value="" selected disabled>Silakan Pilih Tahun</option>
                            <?php
                                foreach ($data_tahun as $data) {
                                    echo "
                                        <option value='$data->tahun'>$data->tahun</option>
                                    ";
                                } 
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12">Tahun Akhir</label>
                    <div class="col-md-12">
                        <select name="tahun_akhir" class="form-control" required>
                            <option value="" selected disabled>Silakan Pilih Tahun</option>
                            <?php
                                foreach ($data_tahun as $data) {
                                    echo "
                                        <option value='$data->tahun'>$data->tahun</option>
                                    ";
                                } 
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button name="submit" value="submit" type="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.row -->

<?php if($view_data == false){}else{ ?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0" >Hasil Perhitungan Data <?php echo strtoupper($tipe_data).' Tahun '.$tawal.'-'.$takhir ?></h3><br><br>
                <?php
                  $j = (sizeof($perhitungan) - 1)/2;
                  $total = array();
                  foreach ($perhitungan['centroid_'.$j] as $key => $value) {
                    $sum = array( $key => array_sum((array) $value));
                    $total += $sum;
                  }
                  arsort($total);
                  $label = array('Tinggi','Sedang','Rendah');
                  $new_label = array();
                  $no = 0;
                  foreach ($total as $key => $value) {
                      $old = array( $key => $label[$no]);
                      $new_label += $old;
                      $no++;
                  }
                ?>
                <table id="satu" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Provinsi</th>
                        <?php 
                        // print_r($perhitungan);
                        $awal = $tawal;
                        $akhir = $takhir;
                        for ($i=$awal; $i <= $akhir; $i++) { 
                        echo "<th>$i</th>";
                        }
                      ?>
                        <th>Cluster</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                          foreach ($perhitungan['gabungan'] as $key => $value) {
                            $min = $value['min'];
                            $cluster = strtoupper($min).' ('.$new_label[$min].')';
                            if ($min == "c1") {
                              $class  = "danger";
                            }elseif ($min == "c2") {
                              $class = "warning";
                            }elseif ($min = "c3") {
                              $class = "info";
                            }
                            echo "<tr class='$class'><td>$key</td>";
                            for ($i=$awal; $i <= $akhir; $i++) { 
                            $qq = $value['thn_'.$i];
                            echo "<td>$qq</td>";
                            }
                            echo "<td>$cluster</td></tr>";
                          }
                        ?>
                    </tbody>
                </table>
        <button type="submit" onclick="display_detail();" class="btn btn-primary">Lihat Detail Perhitungan</button> 
        </div>
    </div>
</div>
<?php } ?>

<div id="detail" class="row" style="display: none">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Detail Perhitungan</h3>
            <?php
                //print_r($perhitungan);
                for ($ii=1; $ii <= $j ; $ii++) {
                echo "<h2>Iterasi $ii</h2><br>";
                ?>
                
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Centroid <?php echo $ii; ?></th>
                        <?php 
                        for ($i=$awal; $i <= $akhir; $i++) { 
                        echo "<th>$i</th>";
                        }
                      ?>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                         $hasil = $perhitungan['centroid_'.$ii];
                          foreach ($hasil as $key => $value) {
                            if ($key == "c1") {
                              $class  = "danger";
                            }elseif ($key == "c2") {
                              $class = "warning";
                            }elseif ($key = "c3") {
                              $class = "info";
                            }
                            echo "
                            <tr class='$class'>
                              <td>$key</td>";
                            for ($i=$awal; $i <= $akhir; $i++) { 
                            $qq = "thn_".$i;
                            $tahun = $value->$qq;
                            echo "
                              <td>$tahun</td>";
                            }
                            echo "
                            </tr>";
                          }
                        ?>
                    </tbody>
                </table><br>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Provinsi</th>
                        <?php 
                        for ($i=$awal; $i <= $akhir; $i++) { 
                        echo "<th>$i</th>";
                        }
                      ?>
                        <th>C1</th>
                        <th>C2</th>
                        <th>C3</th>
                        <th>Cluster</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                         $hasil = $perhitungan['interasi_'.$ii]['hasil'];
                          foreach ($hasil as $key => $value) {
                            $c1 = $value['c1'];
                            $c2 = $value['c2'];
                            $c3 = $value['c3'];
                            $min = $value['min'];
                            if ($min == "c1") {
                              $class  = "danger";
                            }elseif ($min == "c2") {
                              $class = "warning";
                            }elseif ($min = "c3") {
                              $class = "info";
                            }
                            echo "
                            <tr class='$class'>
                              <td>$key</td>";
                            for ($i=$awal; $i <= $akhir; $i++) { 
                            $qq = $value['thn_'.$i];
                            echo "<td>$qq</td>";
                            }
                            echo "
                              <td>$c1</td>
                              <td>$c2</td>
                              <td>$c3</td>
                              <td>$min</td>
                            </tr>";
                          }
                        ?>
                    </tbody>
                </table><br><br><hr>

              <?php
                }
              ?>  
        </div>
    </div>
</div>