<!-- .row -->
<div class="row">
    <center>
      <img class="img-responsive" src="<?php echo base_url('assets/plugins/images/kemenkes.png'); ?>" alt="Home" style="width: 350px;  margin-bottom: 20px;">
    </center>
    <div class="col-sm-12">
        <div class="white-box">
            <img class="img-responsive" src="<?php echo base_url('assets/plugins/images/gambar1.jpg'); ?>" alt="aids" style="float: left; width: 350px; margin-right: 20px; margin-bottom: 20px;">
            <h1>HIV & AIDS</h1>
            <p align="justify">HIV atau Human Immunodeficiency Virus adalah virus yang menyerang sel darah putih di dalam tubuh (limfosit) yang mengakibatkan turunnya kekebalan tubuh manusia. Orang yang dalam darahnya terdapat virus HIV dapat tampak sehat dan belum membutuhkan pengobatan. Namun orang tersebut dapat menularkan virusnya kepada orang lain bila melakukan hubungan seks berisiko dan berbagi alat suntik dengan orang lain. <br><br>
            AIDS atau Acquired Immune Deficiency Syndrome adalah sekumpulan gejala penyakit yang timbul karena turunnya kekebalan tubuh. AIDS disebabkan oleh infeksi HIV. Akibat menurunnya kekebalan tubuh pada seseorang maka orang tersebut sangat mudah terkena penyakit seperti TBC, kandidiasis, berbagai radang pada kulit, paru, saluran pencernaan, otak dan kanker. Stadium AIDS membutuhkan pengobatan Antiretroviral (ARV) untuk menurunkan jumlah virus HIV di dalam tubuh sehingga bisa sehat kembali. <br><br>
            HIV bisa ditularkan kepada orang lain dengan cara:</P>
            <img class="img-responsive" src="<?php echo base_url('assets/plugins/images/gambar2.jpg'); ?>" alt="hiv" style="float: right; width: 350px; margin-left: 20px; margin-bottom: 20px;">

            <ol>
              <li>Melalui hubungan seks tanpa menggunakan kondom sehingga memungkinkan cairan mani atau cairan vagina yang mengandung virus HIV masuk ke dalam tubuh pasangannya</li>
              <li>Dari seorang ibu hamil yang HIV positif kepada bayinya selama masa kehamilan, waktu persalinan dan/atau waktu menyusui.</li>
              <li>Melalui transfusi darah/produk darah yang sudah tercemar HIV. Lewat pemakaian alat suntik yang sudah tercemar HIV, yang dipakai bergantian tanpa disterilkan, terutama terjadi pada pemakaian bersama alat suntik di kalangan pengguna narkoba suntik (penasun).</li>
            </ol>
            <p align="justify">Infeksi HIV dapat dicegah dengan cara:</p>	
            <ol>
              <li>Abstinence = Tidak berhubungan seks (selibat)</li>
              <li>Be Faithful = Selalu setia pada pasangan</li>
              <li>Condom = Gunakan kondom di setiap hubungan seks berisiko</li>
              <li>Drugs =  Jauhi narkoba</li>
            </ol>
            <p align="justify"><br> Untuk Pengobatan HIV dan AIDS pada dasarnya meliputi aspek Medis Klinis, Psikologis dan Aspek Sosial yang meliputi pengobatan supportive (dukungan), pencegahan dan pengobatan infeksi oportunistik dan pengobatan antiretroviral. 
            ARV merupakan singkatan dari Antiretroviral, yaitu obat yang dapat menghentikan reproduksi HIV didalam tubuh. Bila pengobatan tersebut bekerja secara efektif, maka kerusakan kekebalan tubuh dapat ditunda bertahun–tahun dan dalam rentang waktu yang cukup lama sehingga orang yang terinfeksi HIV dapat mencegah AIDS. Dengan semakin meningkatnya jumlah kasus infeksi HIV tersebut, ARV memiliki peran penting dalam menciptakan masyarakat sehat melalui strategi penanggulangan AIDS yang memadukan upaya pencegahan dengan upaya perawatan, dukungan serta pengobatan.
            Hingga saat ini, ARV masih merupakan cara paling efektif serta mampu menurunkan angka kematian dan berdampak pada peningkatan kualitas hidup orang terinfeksi HIV sekaligus meningkatkan harapan masyarakat untuk hidup lebih sehat. Sehingga pada saat ini HIV dan AIDS telah diterima sebagai penyakit yang dapat dikendalikan seperti diabetes, asma atau darah tinggi dan tidak lagi dianggap sebagai penyakit yang pembunuh yang menakutkan</p>
            
        </div>
    </div>   
</div>