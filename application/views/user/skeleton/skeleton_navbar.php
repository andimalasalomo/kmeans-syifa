<body class="mini-sidebar fix-header">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <?php $curr_url = uri_string(); ?>
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="<?php echo base_url("user/index");?>">
                        <b>
                            <img style="width: 40px; " src="<?php echo base_url("assets/plugins/images/ribbon-512.png");?>" alt="hiv aids">
                        </b>
                        <span>
                            K-Means HIV & AIDS
                        </span>
                    </a>
                </div>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="side-menu">
                        <li>
                            <a class="waves-effect <?php if($curr_url == "user/index"){ echo "active";}?>" href="<?php echo base_url("user/index");?>" aria-expanded="false">
                            <i class="fa fa-home fa-fw"></i> <span class="hide-menu">Beranda</span></a>
                        </li>
                        <li>
                            <a class="waves-effect <?php if($curr_url == "user/data" or $curr_url == "user/searchdata"){ echo "active";}?>" href="<?php echo base_url("user/data");?>" aria-expanded="false"><i class="icon-notebook fa-fw"></i> <span class="hide-menu">Data</span></a>
                        </li>
                        <li>
                             <a class="waves-effect <?php if($curr_url == "user/perhitungan"){ echo "active";}?>" href="<?php echo base_url("user/perhitungan");?>" aria-expanded="false"><i class="fa fa-calculator fa-fw"></i> <span class="hide-menu">Pengelompokan</span></a>
                        </li>
                        <li>
                             <a class="waves-effect <?php if($curr_url == "user/tentang") { echo "active";}?>" href="<?php echo base_url("user/tentang");?>" aria-expanded="false"><i class="fa fa-info-circle fa-fw"></i> <span class="hide-menu">Tentang</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="container-fluid">