<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url("assets/plugins/images/ribbon-512.png");?>">
  <title>Login - HIV & AIDS</title>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/vendor/bootstrap/css/bootstrap.min.css");?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/fonts/font-awesome-4.7.0/css/font-awesome.min.css");?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/fonts/iconic/css/material-design-iconic-font.min.css");?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/vendor/animate/animate.css");?>">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/vendor/css-hamburgers/hamburgers.min.css");?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/vendor/animsition/css/animsition.min.css");?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/vendor/select2/select2.min.css");?>">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/vendor/daterangepicker/daterangepicker.css");?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/css/util.css");?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/css/main.css");?>">
<!--===============================================================================================-->
</head>
<body>
  <div class="limiter">
    <div class="container-login100" style="background-image: url("<?php echo base_url("assets/admin/image/bg-o1.jpg");?>")">
      <div class="wrap-login100">
        <form action="<?php echo base_url("login/dologin");?>" method="post" class="login100-form validate-form">
          <?php if($this->session->flashdata('notif')){
          $type = $this->session->flashdata('type');
          echo "<div id='alert' class='alert $type'>";
          echo $this->session->flashdata('notif').'</div>';
      } ?>
          <span class="login100-form-title p-b-34 p-t-27">
            Log in
          </span>

          <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input autofocus class="input100" type="text" name="username" placeholder="Username" required>
            <span class="focus-input100" data-placeholder="&#xf207;"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" placeholder="Password" required>
            <span class="focus-input100" data-placeholder="&#xf191;"></span>
          </div>
          <div class="container-login100-form-btn">
            <button type="submit" name="login" value="login" class="btn btn-primary btn-block btn-flat">Log In</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  

  <div id="dropDownSelect1"></div>
  
<!--===============================================================================================-->
  <script src="<?php echo base_url("assets/admin/vendor/jquery/jquery-3.2.1.min.js");?>"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url("assets/admin/vendor/animsition/js/animsition.min.js");?>"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url("assets/admin/vendor/bootstrap/js/popper.js");?>"></script>
  <script src="<?php echo base_url("assets/admin/vendor/bootstrap/js/bootstrap.min.js");?>"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url("assets/admin/vendor/select2/select2.min.js");?>"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url("assets/admin/vendor/daterangepicker/moment.min.js");?>"></script>
  <script src="<?php echo base_url("assets/admin/vendor/daterangepicker/daterangepicker.js");?>"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url("assets/admin/vendor/countdowntime/countdowntime.js");?>"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url("assets/admin/js/main.js");?>"></script>

</body>
</html>