<body class="mini-sidebar fix-header">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <?php $curr_url = uri_string(); ?>
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="<?php echo base_url("admin/index");?>">
                        <b>
                            <img style="width: 40px; " src="<?php echo base_url("assets/plugins/images/ribbon-512.png");?>" alt="hiv aids">
                        </b>
                        <span>
                            K-Means HIV & AIDS
                        </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="right-side-toggle">
                        <a class="right-side-toggler waves-effect waves-light b-r-0 font-20" href="javascript:void(0)">
                            <li><a href="<?php echo base_url('login/dologout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="side-menu">
                        <li>
                            <a class="waves-effect <?php if($curr_url == "admin/dashboard"){ echo "active";}?>" href="<?php echo base_url("admin/dashboard");?>" aria-expanded="false">
                            <i class="fa fa-home fa-fw"></i> <span class="hide-menu">Dashboard</span></a>
                        </li>
                         
                        
                        <li>
                            <a class="waves-effect <?php if($curr_url == "admin/view" or $curr_url == "admin/data" or $curr_url == "admin/edit" or $curr_url == "admin/add"){ echo "active";}?>" href="<?php echo base_url("admin/data");?>" aria-expanded="false"><i class="icon-notebook fa-fw"></i> <span class="hide-menu">Manajemen Data</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="container-fluid">