    </div>
    <!-- ===== Right-Sidebar-End ===== -->
    </div>
        <!-- /.container-fluid -->
        <footer class="footer t-a-c">
            © 2017 Cubic Admin
        </footer>
    </div>
    <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
   
    <!-- jQuery -->
    <script src="<?php echo base_url("assets/plugins/components/jquery/dist/jquery.min.js");?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="<?php echo base_url("assets/js/sidebarmenu.js");?>"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="<?php echo base_url("assets/js/jquery.slimscroll.js");?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url("assets/js/waves.js");?>"></script>
    <!-- Data Tables -->
    <script src="<?php echo base_url("assets/plugins/components/datatables/jquery.dataTables.min.js");?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url("assets/js/custom.js");?>"></script>
    <!-- Select 2 -->
    <script src="<?php echo base_url("assets/plugins/components/custom-select/custom-select.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo base_url("assets/plugins/components/bootstrap-select/bootstrap-select.min.js");?>" type="text/javascript"></script>

  <script>
  //datatable
  $(function () {
    $('#example1').DataTable({
      'responsive': true,
      'ordering': true,
      'columnDefs': [{
        orderable: false,
        targets: 'no-sort'
      }]
    });
  })

  $('#alert').delay(2500).fadeOut(); 
</script>
</body>

</html>