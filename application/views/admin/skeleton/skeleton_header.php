<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url("assets/plugins/images/ribbon-512.png");?>">
    <title>Admin Page - HIV & AIDS</title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <link href="<?php echo base_url("assets/plugins/components/datatables/jquery.dataTables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("assets/plugins/components/custom-select/custom-select.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("assets/plugins/components/bootstrap-select/bootstrap-select.min.css");?>" rel="stylesheet" />
    <!-- ===== Animation CSS ===== -->
    <link href="<?php echo base_url("assets/css/animate.css");?>" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="<?php echo base_url("assets/css/style.css");?>" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="<?php echo base_url("assets/css/colors/red.css");?>" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>