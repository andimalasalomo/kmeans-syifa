<!-- .row -->
<div class="row">
    <div class="col-md-6">
        <div class="white-box ecom-stat-widget">
            <div class="row">
                <div class="col-xs-6">
                    <span class="text-blue font-light"><?php echo $total_data->jml; ?></i></span>
                    <p class="font-12">Total Tahun</p>
                </div>
                <div class="col-xs-6">
                  <span class="icoleaf bg-primary text-white"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box ecom-stat-widget">
            <div class="row">
                <div class="col-xs-6">
                    <span class="text-blue font-light"><?php echo $total_provinsi->jml; ?></i></span>
                    <p class="font-12">Total Data Provinsi</p>
                </div>
                <div class="col-xs-6">
                       <span class="icoleaf bg-success text-white"><i class="fa fa-map-o"></i></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Selamat Datang di Admin Panel</h3>
            <p class="text-muted m-b-30 font-13"> Cara penggunaan admin panel </p>
            Admin Panel digunakan untuk mengelola data penyakit HIV AIDS di setiap provinsi di Indonesia.
    </div>   
</div>