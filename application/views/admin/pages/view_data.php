<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Data Penyakit HIV dan AIDS</h3>
            <p class="text-muted m-b-30">Data Penyakit Tahun <?php echo($tahun); ?></p
            <div class="table-responsive">
                <table id="example1" class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th class="no-sort" style="width: 350px">Provinsi</th>
                            <th class="no-sort" >HIV</th>
                            <th class="no-sort" >AID</th>
                            <th>Tahun</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                    $no = 1;
                    foreach ($data_edit as $data) {
                        $provinsi = $data->provinsi;
                        $hiv = $data->hiv;
                        $aids = $data->aids;
                        $tahun = $data->tahun;
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $provinsi ?></td>
                            <td><?php echo $hiv ?></td>
                            <td><?php echo $aids ?></td>
                            <td><?php echo $tahun ?></td>         
                        </tr>
                        <?php
                    }
                ?>                          
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->