<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Data Penyakit HIV dan AIDS <a href="<?php echo base_url("admin/add") ?>" class="btn btn-primary pull-right"><i class="fa fa-upload"></i>&nbsp;&nbsp;Tambah Data</a></h3>
            <p class="text-muted m-b-30"> Daftar Penyakit Tercatat</p>
            <?php if($this->session->flashdata('notif')){
                $type = $this->session->flashdata('type');
                echo "<div id='alert' class='alert $type'>";
                echo $this->session->flashdata('notif').'</div>';
            } ?><br>
            <div class="table-responsive">
                <table id="example1" class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tahun</th>
                            <th style="width: 500px" class="no-sort">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                            $no = 1;
                            foreach ($data_tahun as $data) {
                        $tahun = $data->tahun;
                        ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $tahun ?></td>
                                <td>
                                <div class="row">
                                <div class="col-md-2">
                                <form action="<?php echo base_url('admin/view'); ?>" method="post">
                                    <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
                                    <button type="submit" name="confirm" class="btn btn-warning btn-outline"><i class="fa fa-eye"></i> View</button>
                                </form>
                                </div>  
                                <div class="col-md-2">
                                <form action="<?php echo base_url('admin/edit'); ?>" method="post">
                                    <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
                                    <button type="submit" name="confirm" value="confirm" class="btn btn-primary btn-outline"><i class="fa fa-edit"></i> Edit</button>
                                </form>
                                </div>
                                <div class="col-md-2">
                                <form action="" method="post" onsubmit="return confirm('Yakin menghapus data?');">
                                    <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
                                    <button type="submit" name="deleteData" value="deleteData" class="btn btn-danger btn-outline"></i><i class="fa fa-trash"></i> Delete</button>
                                </form>
                              </div>
                              </div>
                                </td>
                            </tr>
                        <?php        
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->