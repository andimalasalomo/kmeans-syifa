
<!-- /row -->
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Data Penyakit HIV dan AIDS</h3>
            <p class="text-muted m-b-30">Edit data Penyakit Tahun <?php echo($tahun); ?></p
            <div class="table-responsive">
                <form action="<?php echo base_url("admin/doeditdata");?>" method="post" >
              <div class="box-body">
                  <?php if($this->session->flashdata('notif')){
                        $type = $this->session->flashdata('type');
                        echo "<div id='alert' class='alert $type'>";
                        echo $this->session->flashdata('notif').'</div>';
                    } ?>
                  <div class="row">   
                    <div class="col-md-12">
                      <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Tahun</label>
                            <div class="col-sm-11">
                              <input value="<?php echo($tahun);  ?>" type="number" name="tahun" class="form-control" placeholder="Masukan Tahun">
                            </div>
                        </div>
                      </div>
                    </div>   
                  </div>
                  <div class="row">
                      <table class="table table-responsive">
                        <tr>
                          <th>No</th>
                          <th>Provinsi</th>
                          <th>HIV</th>
                          <th>AIDS</th>                     
                        </tr> 
                        <?php
                          $i = 1;
                          foreach ($data_edit as $data) {
                            echo "
                          <tr>
                          <td>$i</td>
                          <td> 
                            $data->provinsi
                            <input type='hidden' name='provinsi[$i]' value='$data->provinsi'>
                            <input type='hidden' name='id_data[$i]' value='$data->id_data'>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='hiv[$i]' value='$data->hiv' class='form-control' placeholder='hiv'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='aids[$i]' value='$data->aids' class='form-control' placeholder='aids'>
                            </div>
                          </td>
                          </tr>";
                            $i++;
                          }
                        ?>                          
                        
                      </table>
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="data" class="btn btn-danger">Cancel</a>
              </div>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->