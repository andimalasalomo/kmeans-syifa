<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {

    function login($data){
        $this->db->select('*');
        $this->db->from('akun');
        $this->db->where('username', $data['username']);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            $hash = $query->row()->hash;
            if(password_verify($data['password'], $hash)){
                unset($data['password']);
                return $query->result();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}

/* End of file M_auth.php */
