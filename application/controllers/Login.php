<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('M_auth');
        $this->load->model('M_global');
	}

    public function index(){
        $this->load->view('login');
    }
    public function dologin(){
        if($this->input->post('login')){

            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
        );
                    
            $result = $this->M_auth->login($data);
            print_r($result);

            if($result){

                $sess_array = array();
                foreach($result as $row) {
                    $sess_array = array(
                        'ID'        => $row->id_akun,
                        'LOGIN'     => TRUE
                    );
                }

                $this->session->set_userdata($sess_array);
                redirect(base_url("admin/dashboard"));

            }else{
                $this->session->set_flashdata('type','alert-danger');
                $this->session->set_flashdata('notif','Login Gagal!! Username atau Password Salah !');
                redirect(base_url("login"));                
            }

        }else{
			redirect(base_url("login"));
		}
    }
    public function dologout(){
        $sess_array = array(
            'ID'    => '',
            'LOGIN' => FALSE
        );

        $this->session->unset_userdata($sess_array);
        session_destroy();
        redirect(base_url("login"));
    }

}

/* End of file Login.php */
