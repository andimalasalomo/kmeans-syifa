<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('M_global');
        $halaman = $this->uri->segment(2);
        if ( !in_array($halaman, array('login'), true ) ) {
            if($this->session->userdata('LOGIN')!=TRUE){
                $this->session->set_flashdata('type','alert-warning');
                $this->session->set_flashdata('notif','Harap login terlebih dahulu.');
                redirect(base_url("login"));
            }
        }
    }

    public function index(){        
        redirect(base_url("admin/dashboard"));        
    }
    public function dashboard(){
        $data['total_data'] = $this->M_global->countAll('COUNT(*) as jml','tahun');
        $data['total_provinsi'] = $this->M_global->countAll('count(DISTINCT(provinsi)) as jml','provinsi');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/dashboard',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }
    public function data(){

        if($this->input->post('deleteData')){
            $tahun = $this->input->post('tahun');
            $where = array(
                'tahun' => $tahun
            );
            $delete = $this->M_global->delete('data_hiv_aids',$where);
            $delete_tahun = $this->M_global->delete('tahun',$where);
            echo "<script>alert('Data berhasil dihapus.')</script>";
            echo "<script> window.location.href = ''</script>";
        }
        

        $data['data_tahun'] = $this->M_global->get('tahun','','*');
        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/manage_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }
    public function add(){
        
        $data['provinsi'] = $this->M_global->get('provinsi','','*');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/add_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }
    public function doinsertdata(){
        $index = 1;
        $post = $this->input->post();

        $total = sizeof($post['provinsi']);
        $data = array();
        for ($i=1; $i <= $total; $i++) { 
          $dump = array(
            'provinsi' => $post['provinsi'][$i],
            'hiv' => $post['hiv'][$i],
            'aids' => $post['aids'][$i],
            'tahun' => $post['tahun'],
           );
           array_push($data,$dump);
        }
        $where = array(
              'tahun' => $post['tahun'],
            );
        $save = $this->M_global->insertBatch('data_hiv_aids',$data);
        $save1 = $this->M_global->insert('tahun',$where);
        if ($save > 0) {
            $this->session->set_flashdata('type','alert-success');
            $this->session->set_flashdata('notif','Data berhasil disimpan.');
        }else{
            $this->session->set_flashdata('type','alert-danger');
            $this->session->set_flashdata('notif','Data gagal disimpan, Silakan ulangi !!!');
        }
        redirect('admin/data');
        
    }
    public function edit(){
        $where = array(
            'tahun' => $this->input->post('tahun')
        );
        $data['data_edit'] = $this->M_global->get('data_hiv_aids',$where,'*');
        $data['tahun'] = $this->input->post('tahun');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/edit_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }

    public function doeditdata(){
        $post = $this->input->post();
        $total = sizeof($post['provinsi']);
        $data = array();
        for ($i=1; $i <= $total; $i++) { 
          $dump = array(
            'id_data' => $post['id_data'][$i],  
            'provinsi' => $post['provinsi'][$i],
            'hiv' => $post['hiv'][$i],
            'aids' => $post['aids'][$i],
           );
           array_push($data,$dump);
        }
        $save = $this->M_global->updateBatch('data_hiv_aids',$data,'id_data');
        if ($save > 0) {
            $this->session->set_flashdata('type','alert-success');
            $this->session->set_flashdata('notif','Data berhasil diupdate.');
        }else{
            $this->session->set_flashdata('type','alert-warning');
            $this->session->set_flashdata('notif','Data gagal diupdate atau tidak terjadi perubahan');
        }
        redirect('admin/data');
        
    }

    public function view(){
        $where = array(
            'tahun' => $this->input->post('tahun')
        );
        $data['data_edit'] = $this->M_global->get('data_hiv_aids',$where,'*');
        $data['tahun'] = $this->input->post('tahun');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/view_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }


}
