<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('M_global');
	}

    public function index(){
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/home');
        $this->load->view('user/skeleton/skeleton_footer');     
    }

    public function data(){
        $data['viewdata'] ="";
        $data['data_tahun'] = $this->M_global->get('tahun','','DISTINCT(tahun)');
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/data',$data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }

    public function tentang(){
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/tentang');        
        $this->load->view('user/skeleton/skeleton_footer');
    }
     public function searchdata(){
        $thnawal = $this->input->post('thn_awal');
        $thnakhir = $this->input->post('thn_akhir');
        
        if($thnawal <= $thnakhir){
            $data['viewdata'] ="yes";
           $data['data_edit']  = $this->db->query("SELECT * FROM data_hiv_aids WHERE tahun BETWEEN '$thnawal' AND '$thnakhir'")->result();
        }
        else{
            $data['viewdata'] ="error"; 
        }
        $data['thn_awal'] = $thnawal;
        $data['thn_akhir'] = $thnakhir;
        $data['data_tahun'] = $this->M_global->get('tahun','','DISTINCT(tahun)');
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/data',$data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }
    public function perhitungan(){
        $data['data_tahun'] = $this->M_global->get('data_hiv_aids','','DISTINCT(tahun)');
        $data['view_data'] = false;
        $submit = $this->input->post('submit');
        if (isset($submit)) {
            $tipe_data = $this->input->post('tipe_data');
            $tawal = $this->input->post('tahun_awal');
            $takhir = $this->input->post('tahun_akhir');
            if ($tawal >= $takhir ) {
                $this->session->set_flashdata('type','alert-danger');
                $this->session->set_flashdata('notif','Format data yang anda masukan salah silakan ulangi');
            }else{
                if (isset($_SESSION['type']) or isset($_SESSION['notif'])) {
                    unset($_SESSION['type']);
                    unset($_SESSION['notif']);
                }
                $data['tipe_data'] = $tipe_data;
                $data['tawal'] = $tawal;
                $data['takhir'] = $takhir;
                $data['perhitungan'] = $this->perhitungan_raw($tipe_data,$tawal,$takhir,'23','2','12');
                $data['view_data'] = true;
            }
            
        }

        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/perhitungan',$data);  
        $this->load->view('user/skeleton/skeleton_footer');
    }

    public function perhitungan_raw($tipe_data , $tawal , $takhir, $qq , $ww , $ee ){
        function centroid($c1, $c2, $c3, $data, $thnawal, $thnakhir){
            $c[1]  = $c1;
            $c[2]  = $c2;
            $c[3]  = $c3;
            for($yy=1;$yy<=3;$yy++){
                $hasil[$yy] = array();
                foreach ($data as $key) {
                    $arrayData = array();
                    foreach ($key as $value) {
                        array_push($arrayData, $value);
                    }
                    $arrayCent = array();
                    foreach ($c[$yy] as $cval) {
                        array_push($arrayCent, $cval);
                    }
                    $x = 0;
                    for($i=1;$i < (sizeof($arrayData) - 1);$i++){
                        $y[$i] = $arrayData[$i] - $arrayCent[$i];
                        $x += pow($y[$i],2);
                    }
                    $result = sqrt($x);
                    array_push($hasil[$yy], $result);
                }
            }

            $no = 0;
            foreach($data as $val){
                $provinsi = $val->prov;
                $min = array(
                    "c1" => $hasil[1][$no],
                    "c2" => $hasil[2][$no],
                    "c3" => $hasil[3][$no]
                );
                $min_h = array_keys($min, min($min));
                
                foreach($data as $provs){
                    for($tahun=$thnawal;$tahun<=$thnakhir;$tahun++){
                        $key_thn = 'thn_'.$tahun;
                        $data_tahun['thn_'.$tahun] = $provs->$key_thn;
                    }
                    $data_prov[$provs->prov] = $data_tahun;
                }

                $data_centroid[$val->prov] = array(
                    "c1" => $hasil[1][$no],
                    "c2" => $hasil[2][$no],
                    "c3" => $hasil[3][$no],
                    "min" => $min_h[0]
                );
                $prv = $data_prov[$val->prov];
                $cnt = $data_centroid[$val->prov];
                $new[$val->prov] = $prv + $cnt;

                $no++;
            }
            $no = 0;
            foreach($data as $val){
                $prov = $val->prov;
                $min = array(
                    "c1" => $hasil[1][$no],
                    "c2" => $hasil[2][$no],
                    "c3" => $hasil[3][$no]
                );

                $min_h = array_keys($min, min($min));

                $t[$prov] = array(
                    "min" => $min_h[0]);
                $no++;
            }

            $c1=0;
            $c2=0;
            $c3=0;
            foreach($new as $val){
                $asu[] = $val['min'];
            }
            $counts = array_count_values($asu);

            $output = array(
                "hasil" => $new,
                "t" => $t,
                "c1" => $counts['c1'],
                "c2" => $counts['c2'],
                "c3" => $counts['c3']
            );
            return $output;
        }

        function iterasi($thnawal,$thnakhir,$data){
            for($i=1; $i<=3; $i++){
                
                for($tahun=$thnawal;$tahun<=$thnakhir;$tahun++){
                    ${"thn_$tahun"} = 0;
                }
                
                foreach($data['hasil'] as $obj){
                    if($obj['min'] == 'c'.$i){
                        for($tahun=$thnawal;$tahun<=$thnakhir;$tahun++){
                            ${"thn_$tahun"} = ${"thn_$tahun"} + $obj['thn_'.$tahun];
                        }
                    }
                }
                
                $ch['prov'] = '';
                for($tahun=$thnawal;$tahun<=$thnakhir;$tahun++){
                    $ch['thn_'.$tahun] = ${"thn_$tahun"} / $data['c'.$i];
                }
                
                $c[$i] = array(
                    $ch
                );
            }
            $hasil = array(
                "c1" => $c[1],
                "c2" => $c[2],
                "c3" => $c[3]
            );
            return $hasil;
        }

        $thnawal = $tawal;
        $thnakhir = $takhir;

        $jenis = $tipe_data;

        $qTahun = '';
        for($tahun=$thnawal;$tahun<=$thnakhir;$tahun++){
            $qTahun .= "MAX(CASE WHEN tahun = '$tahun' THEN $jenis END) thn_$tahun,";
        }

        $data = $this->M_global->custom("
            SELECT provinsi as prov,
                $qTahun
                (SELECT SUM(aids) FROM data_hiv_aids WHERE provinsi = prov GROUP BY provinsi) AS jumlah
            FROM data_hiv_aids
            GROUP BY provinsi
        ");
        
        //Iterasi pertama
        $c1 = $data[$qq];
        $c2 = $data[$ww];
        $c3 = $data[$ee];
        $centroid[1] = centroid($c1, $c2, $c3, $data, $thnawal, $thnakhir);
        $iterasi[1] = iterasi($thnawal, $thnakhir, $centroid[1]);
        //print_r($centroid[1]);
        //print_r($iterasi[1]);
        $returned['interasi_1'] = $centroid[1];
        $returned['centroid_1'] = array(
            'c1' => $c1,
            'c2' => $c2,
            'c3' => $c3,
        );

        //Iterasi kedua
        $c1  = (object) $iterasi[1]['c1'][0];
        $c2  = (object) $iterasi[1]['c2'][0];
        $c3  = (object) $iterasi[1]['c3'][0];
        $centroid[2] = centroid($c1, $c2, $c3, $data, $thnawal, $thnakhir);
        $iterasi[2] = iterasi($thnawal, $thnakhir, $centroid[2]);
        //print_r($centroid[2]);
        //print_r($iterasi[2]);
        $returned['interasi_2'] = $centroid[2];
        $returned['centroid_2'] = array(
            'c1' => $c1,
            'c2' => $c2,
            'c3' => $c3,
        );

        if($centroid[1]['t'] == $centroid[2]['t']){
            $gabungan = $centroid[2]['hasil'];
            function sort_by_name($a,$b){
                return $a["min"] > $b["min"];
            }
            uasort($gabungan,"sort_by_name");
            $returned['gabungan'] = $gabungan;
            return $returned;
        }else{
            //Iterasi ke-x
            for($i=2;$i<=100;$i++){
                $r = $i + 1;
                $c1  = (object) $iterasi[$i]['c1'][0];
                $c2  = (object) $iterasi[$i]['c2'][0];
                $c3  = (object) $iterasi[$i ]['c3'][0];
                $centroid[$r] = centroid($c1, $c2, $c3, $data, $thnawal, $thnakhir);
                $iterasi[$r] = iterasi($thnawal, $thnakhir, $centroid[$r]);
                //print_r($centroid[$r]);
                //print_r($iterasi[$r]);
                $returned['interasi_'.$r] = $centroid[$r];
                $returned['centroid_'.$r] = array(
                        'c1' => $c1,
                        'c2' => $c2,
                        'c3' => $c3,
                    );
                if($centroid[$i]['t'] == $centroid[$r]['t']){
                    //print_r($centroid[$r]['hasil']);
                    $gabungan = $centroid[$r]['hasil'];
                    
                    function sort_by_name($a,$b){
                        return $a["min"] > $b["min"];
                    }
                    
                    uasort($gabungan,"sort_by_name");
                    //print_r($gabungan);
                    $returned['gabungan'] = $gabungan;
                    return $returned;
                    break;
                }else{
                }
            }
        }

    }


}

/* End of file User.php */
